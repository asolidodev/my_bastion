resource "aws_security_group" "bastion" {
  count  = var.my_bastion_create ? 1 : 0
  vpc_id = var.my_bastion_vpc_id
  name   = "${var.my_bastion_organization}_${var.my_bastion_environment}_${var.my_bastion_domain}_${terraform.workspace}_sg_bastion"
  # As of now using the description means an update to it will force a destroy-create.
  # So we refrain from using the description argument.

  tags = {
    ManagedByTerraform = "true"
    TerraformWorkspace = terraform.workspace
    Name               = "${var.my_bastion_organization}_${var.my_bastion_environment}_${var.my_bastion_domain}_${terraform.workspace}_sg_bastion"
  }

  # Inbound Rules
  ## Allow incoming TCP connections
  ## to the default SSH port (22)
  ## from everywhere
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Outbound Rules

  ## Allow outgoing TCP connections
  ## to the default SSH port (22)
  ## to everywhere in the VPC
  egress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = [var.my_bastion_vpc_cidr]
  }

  # allow outgoing ICMP to any server
  egress {
    protocol    = "icmp"
    from_port   = "-1"
    to_port     = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outgoing UDP to NTP port (123) on any server
  egress {
    from_port   = 123
    to_port     = 123
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outgoing TCP connection to DNS port (53) on any server
  egress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outgoing TCP connection to HTTP port (80) on any machine (to allow obtaining updates via HTTP)
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outgoing TCP connection to HTTPS port (443) on any machine (to allow obtaining updates via HTTPS)
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outgoing connection to syslog-tls port (6514) on any machine (to allow sending logs anywhere)
  egress {
    from_port   = 6514
    to_port     = 6514
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outgoing connection to puppet port (8140) in puppet master network
  egress {
    protocol    = "tcp"
    from_port   = 8140
    to_port     = 8140
    cidr_blocks = ["${var.my_bastion_puppet_master_ip}/32"]
  }

  # allow outgoing TCP connection to rabbitmq port (61613) and activemq port (61614) in puppet master network
  egress {
    protocol    = "tcp"
    from_port   = 61613
    to_port     = 61614
    cidr_blocks = ["${var.my_bastion_puppet_master_ip}/32"]
  }

}
