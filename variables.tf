variable "my_bastion_organization" { type = string }
variable "my_bastion_environment" { type = string }
variable "my_bastion_domain" { type = string }
variable "my_bastion_create" {
  type        = string
  description = "Whether to create the bastion(s) and the associated resources"
}
variable "my_bastion_vpc_id" {
  type        = string
  description = "The VPC id where to place the bastion(s)"
}
variable "my_bastion_vpc_cidr" {
  type        = string
  description = "The VPC CIDR where the bastion(s) placed"
  default     = "10.0.0.0/16"
}
variable "my_bastion_puppet_master_ip" {
  type        = string
  description = "The puppet master IP is needed in the security group configuration, to restrict outgoing connections to puppet and mco ports to this IP."
}
